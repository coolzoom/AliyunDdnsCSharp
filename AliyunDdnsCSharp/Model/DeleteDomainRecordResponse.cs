﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AliyunDdnsCSharp.Model
{
    public class DeleteDomainRecordResponse : BaseResponse
    {
        public string RecordId { get; set; }
    }
}
